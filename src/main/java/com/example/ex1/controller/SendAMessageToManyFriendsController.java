package com.example.ex1.controller;

import com.example.ex1.domain.FriendshipDto;
import com.example.ex1.domain.User;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.service.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SendAMessageToManyFriendsController {

    private Service service;
    private User user;
    ObservableList<FriendshipDto> users = FXCollections.observableArrayList();
    ObservableList<FriendshipDto> sendList = FXCollections.observableArrayList();

    @FXML
    private TableView<FriendshipDto> tableViewFriends;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnUserName;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnFirstName;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnLastName;
    @FXML
    private TableView<FriendshipDto> tableViewFriends1;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnUserName1;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnFirstName1;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnLastName1;
    @FXML
    private TextField textFieldSearch;
    @FXML
    private Button buttonAddToList;
    @FXML
    private Button buttonRemove;
    @FXML
    private TextField textFieldMessage;


    public void initialize(){
        tableColumnUserName.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("username"));
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("lastName"));
        tableColumnUserName1.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("username"));
        tableColumnFirstName1.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("firstName"));
        tableColumnLastName1.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("lastName"));

        tableViewFriends.setItems(users);
        tableViewFriends1.setItems(sendList);

        textFieldSearch.textProperty().addListener(o -> handleFilter());
    }

    @FXML
    private void clickButtonAddToList(){
        try{
            if(this.tableViewFriends.getSelectionModel().getSelectedItem() == null){
                throw new ValidationException(" No friend was selected!");
            }
            FriendshipDto fr = this.tableViewFriends.getSelectionModel().getSelectedItem();
            sendList.add(fr);
            users.remove(fr);
        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    @FXML
    private void clickButtonSend(){
        try{
            String messageToSend = textFieldMessage.getText();
            List<User> destinationList = new ArrayList<User>();
            for(FriendshipDto fsd: sendList){
                destinationList.add(service.getUserByUserName(fsd.getUsername()));
            }
            this.service.addMessage(user, destinationList, messageToSend, LocalDateTime.now(), 0L);
            this.textFieldMessage.clear();
            users.addAll(sendList);
            sendList.clear();
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO"," * The message was sent succesfully!");
        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    @FXML
    private void clickButtonRemove(){
        try{
            if(this.tableViewFriends1.getSelectionModel().getSelectedItem() == null){
                throw new ValidationException(" No friend was selected!");
            }
            FriendshipDto fr = this.tableViewFriends1.getSelectionModel().getSelectedItem();
            sendList.remove(fr);
            users.add(fr);
        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    public void setAtributes(Service service, User user){
        this.service = service;
        this.user = user;
        users.setAll(this.getFriendshipDtoList());
    }

    private List<FriendshipDto> getFriendshipDtoList(){
        List<FriendshipDto> friendshipsToReturn = StreamSupport.stream(this.service.getAllFriendshipsOfAnUser(user.getId()).spliterator(), false)
                .map(x->new FriendshipDto(x.getLeft().getUsername(), x.getLeft().getFirstName(), x.getLeft().getLastName(), x.getRight()))
                .collect(Collectors.toList());
        return friendshipsToReturn;
    }

    private void handleFilter() {
        Predicate<FriendshipDto> p1 = n -> n.getUsername().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getLastName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getFirstName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase());
        users.setAll(getFriendshipDtoList()
                .stream()
                .filter(p1)
                .collect(Collectors.toList()));
    }

    @FXML
    private void clickButtonInfo(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO","   * Here you can send a message to many friends.\n   * Select them to create a destinaton list!");
    }


}
