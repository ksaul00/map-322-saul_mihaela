package com.example.ex1.controller;

import com.example.ex1.domain.*;
import com.example.ex1.service.Service;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static javafx.collections.FXCollections.observableArrayList;

public class ShowFriendshipRequestHistoryController {

    private Service service;
    private User user;
    ObservableList<FriendshipRequestSourceDto> friendshipRequests = observableArrayList();

    @FXML
    private TableView<FriendshipRequestSourceDto> tableViewFriendshipRequests;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, String> columnUserName;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, String> columnFirstName;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, String> columnLastName;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, String> columnStatus;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, LocalDate> columnDate;
    @FXML
    private TextField textFieldSearch;
    @FXML
    private ComboBox comboBoxStatus;

    @FXML
    private void clickButtonInfo(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO","   * Here are all your friendship requests!");
    }

    public void initialize(){
        columnUserName.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, String>("fromUserName"));
        columnFirstName.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, String>("fromFirstName"));
        columnLastName.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, String>("fromLastName"));
        columnStatus.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, String>("status"));
        columnDate.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, LocalDate>("date"));
        comboBoxStatus.setItems(observableArrayList("ALL", "PENDING", "REJECTED", "APPROVED"));

        tableViewFriendshipRequests.setItems(friendshipRequests);

        textFieldSearch.textProperty().addListener(o -> handleFilter());
        comboBoxStatus.getSelectionModel().selectedItemProperty().addListener(
                o->handleFilter()
        );
    }

    public void setAtributes(Service service, User user){
        this.service = service;
        this.user = user;
        friendshipRequests.setAll(this.getFriendshipRequests());
    }

    private List<FriendshipRequestSourceDto> getFriendshipRequests(){
        List<FriendshipRequestSourceDto> friendshipRequestsToReturn = StreamSupport.stream(this.service.getFriendshipRequestsOfAnUser(user.getId()).spliterator(), false)
                .map(x->{
                    User user1 = service.getUser(x.getIdSource());
                    return new FriendshipRequestSourceDto(user1.getUsername(), user1.getFirstName(), user1.getLastName(), x.getStatusFriendshipRequest().toString(), x.getDate());
                })
                .collect(Collectors.toList());
        return friendshipRequestsToReturn;
    }

    private void handleFilter() {
        Predicate<FriendshipRequestSourceDto> p2;
        if(comboBoxStatus.getSelectionModel().getSelectedItem().equals("ALL")){
            p2 = n -> n.getStatus()!=null;
        }
        else{
            p2 = n -> n.getStatus().equals(comboBoxStatus.getSelectionModel().getSelectedItem());
        }
        Predicate<FriendshipRequestSourceDto> p1 = n -> n.getFromUserName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getFromLastName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getFromFirstName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase());
        friendshipRequests.setAll(getFriendshipRequests()
                .stream()
                .filter(p1.and(p2))
                .collect(Collectors.toList()));
    }
}
