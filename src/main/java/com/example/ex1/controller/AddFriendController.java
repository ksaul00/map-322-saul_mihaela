package com.example.ex1.controller;

import com.example.ex1.domain.FriendshipRequest;
import com.example.ex1.domain.User;
import com.example.ex1.domain.UserDto;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.service.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AddFriendController {

    private Service service;
    private User user;
    ObservableList<UserDto> users = FXCollections.observableArrayList();

    @FXML
    private TableView<UserDto> tableViewFriends;
    @FXML
    private TableColumn<UserDto, String> tableColumnUserName;
    @FXML
    private TableColumn<UserDto, String> tableColumnFirstName;
    @FXML
    private TableColumn<UserDto, String> tableColumnLastName;
    @FXML
    private TextField textFieldSearch;
    @FXML
    private Button buttonRemoveFriend;


    @FXML
    private void clickButtonInfo(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO","   * Here are the users that are not your friends, \n  that didn't send you a friend request or \n  that didn't recieve a friend request from you.\n   * Press 'Send a request' to make a new friend!");
    }

    public void initialize(){
        tableColumnUserName.setCellValueFactory(new PropertyValueFactory<UserDto, String>("username"));
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<UserDto, String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<UserDto, String>("lastName"));

        tableViewFriends.setItems(users);

        textFieldSearch.textProperty().addListener(o -> handleFilter());
    }

    public void setAtributes(Service service, User user){
        this.service = service;
        this.user = user;
        users.setAll(this.getFriendsSuggestions());
    }

    private List<UserDto> getFriendsSuggestions(){
        return this.service.getAllNonFriendOfAnUser(user)
                .stream()
                .map(x->new UserDto(x.getUsername(), x.getFirstName(), x.getLastName()))
                .collect(Collectors.toList());
    }

    private void handleFilter() {
        Predicate<UserDto> p1 = n -> n.getUsername().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getLastName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getFirstName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase());
        users.setAll(getFriendsSuggestions()
                .stream()
                .filter(p1)
                .collect(Collectors.toList()));
    }

    @FXML
    private void clickButtonSendRequest(){
        try{
            UserDto ur = this.tableViewFriends.getSelectionModel().getSelectedItem();
            if(this.tableViewFriends.getSelectionModel().getSelectedItem() == null){
                throw new ValidationException(" No user was selected!");
            }
            Long id1 = this.user.getId();
            Long id2 = this.service.getUserByUserName(ur.getUsername()).getId();
            FriendshipRequest friendshipRequest = this.service.sendFriendShipRequest(id1, id2, "PENDING", LocalDate.now());
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION," INFO "," Request was sent succesfully!");
            this.users.remove(ur);
        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }


}
