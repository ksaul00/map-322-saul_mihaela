package com.example.ex1.controller;

import com.example.ex1.domain.Message;
import com.example.ex1.domain.User;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ChatController {

    private Service service;
    private User user;
    private User userDestination;
    private Long replyMessageId;
    private AnchorPane menuChat;
    private BorderPane mainPane;

    @FXML
    private Button buttonSend;
    @FXML
    private TextField textFieldMessage;
    @FXML
    private VBox vBoxMessage;
    @FXML
    private ScrollPane scrollPaneMessage;
    @FXML
    private ScrollPane scrollPaneReply;
    @FXML
    private VBox vBoxReply;
    @FXML
    private Button xButton;
    @FXML
    private Label labelChatWith;
    @FXML
    private void clickButtonSend(){
        try{
            String messageToSend = textFieldMessage.getText();
            List<User> destinationList = new ArrayList<User>();
            destinationList.add(userDestination);
            if(this.replyMessageId == null){
                showMessageAsSource(messageToSend, LocalDateTime.now());
                this.service.addMessage(user, destinationList, messageToSend, LocalDateTime.now(), 0L);
            }
            else{
                showReplyForSource(service.getMessage(replyMessageId).getMessage(), messageToSend, LocalDateTime.now());
                this.service.addMessage(user, destinationList, messageToSend, LocalDateTime.now(), replyMessageId);
            }
            this.scrollPaneReply.setVisible(false);
            this.xButton.setVisible(false);
            this.replyMessageId = null;
        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    @FXML
    private void clickButtonGoBack(){
        this.mainPane.setCenter(menuChat);
    }

    @FXML
    private void clickXButton(){
        xButton.setVisible(false);
        this.scrollPaneReply.setVisible(false);
        this.replyMessageId = null;
    }

    @FXML
    private void clickButtonInfo(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO","   * Here you can chat with "+ userDestination.getUsername() + "! Say 'Hello'!");
    }

    public void initialize(){
        vBoxMessage.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                scrollPaneMessage.setVvalue((Double) newValue);
            }
        });
    }

    private void showMessageAsSource(String messageToSend, LocalDateTime date){
        if(!messageToSend.isEmpty()){
            HBox hBox = new HBox();
            hBox.setAlignment(Pos.CENTER_RIGHT);
            hBox.setPadding(new Insets(5, 5, 5, 10));

            javafx.scene.text.Text text = new Text(messageToSend);
            TextFlow textFlow = new TextFlow(text);
            textFlow.setStyle("-fx-color: rgb(239,242,255)" +
                    ";-fx-background-color: rgb(15,125,242)" +
                    ";-fx-background-radius: 20px");
            textFlow.setPadding(new Insets(5, 10, 5, 10));
            text.setFill(Color.color(0.934,0.945, 0.996));
            hBox.getChildren().add(textFlow);
            vBoxMessage.getChildren().add(hBox);
            HBox hBoxDateTime = new HBox();
            hBoxDateTime.setAlignment(Pos.TOP_RIGHT);
            hBoxDateTime.setPadding(new Insets(5, 5, 5, 10));
            Label dateLabel = new Label(date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
            dateLabel.setFont(Font.font("Arial", 9));
            hBoxDateTime.getChildren().add(dateLabel);
            vBoxMessage.getChildren().add(hBoxDateTime);
            textFieldMessage.clear();
        }
    }

    private void showMessageAsFromDestinaton(Long id, String messageToSend, LocalDateTime date){
        if(!messageToSend.isEmpty()){
            if(!messageToSend.isEmpty()){
                HBox hBox = new HBox();
                hBox.setAlignment(Pos.CENTER_LEFT);
                hBox.setPadding(new Insets(5, 5, 5, 10));

                javafx.scene.text.Text text = new Text(messageToSend);
                TextFlow textFlow = new TextFlow(text);
                textFlow.setStyle("-fx-color: rgb(0,0,0)" +
                        ";-fx-background-color: rgb(233,233,260)" +
                        ";-fx-background-radius: 20px");
                textFlow.setPadding(new Insets(5, 10, 5, 10));
                text.setFill(Color.color(0,0, 0));
                hBox.getChildren().add(textFlow);
                Button reply = new Button("->");
                reply.setStyle("-fx-background-color: Pink");
                reply.setOpacity(0.4);
                reply.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event){
                        xButton.setVisible(true);
                        replyMessageId = id;
                        scrollPaneReply.setVisible(true);
                        HBox hBox1 = new HBox();
                        hBox1.setAlignment(Pos.CENTER_LEFT);
                        hBox1.setPadding(new Insets(5, 5, 5, 10));

                        javafx.scene.text.Text text = new Text(service.getMessage(id).getMessage());
                        TextFlow textFlow = new TextFlow(text);
                        textFlow.setStyle("-fx-color: rgb(0,0,0)" +
                                ";-fx-background-color: rgb(233,233,260)" +
                                ";-fx-background-radius: 20px");
                        textFlow.setPadding(new Insets(5, 10, 5, 10));
                        text.setFill(Color.color(0,0, 0));
                        hBox1.getChildren().add(new Label("Reply to: "));
                        hBox1.getChildren().add(textFlow);
                        vBoxReply.getChildren().setAll(hBox1);
                    }

                });
                hBox.getChildren().add(reply);
                vBoxMessage.getChildren().add(hBox);
                HBox hBoxDateTime = new HBox();
                hBoxDateTime.setAlignment(Pos.TOP_LEFT);
                hBoxDateTime.setPadding(new Insets(5, 5, 5, 10));
                Label dateLabel = new Label(date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
                dateLabel.setFont(Font.font("Arial", 9));
                hBoxDateTime.getChildren().add(dateLabel);
                vBoxMessage.getChildren().add(hBoxDateTime);
                textFieldMessage.clear();
            }
        }
    }

    public void setAll(Service service, User user, User userDestination, BorderPane mainPane, AnchorPane menuChat){
        this.service = service;
        this.user = user;
        this.userDestination = userDestination;
        this.replyMessageId = null;
        this.mainPane = mainPane;
        this.menuChat = menuChat;
        for(Message msg: this.service.getMessagesBetween(user.getId(), userDestination.getId())){
            if(msg.getReply()==0)
                if(msg.getFrom().getId().equals(user.getId())){
                    showMessageAsSource(msg.getMessage(), msg.getDate());
                }
                else{
                    showMessageAsFromDestinaton(msg.getId(), msg.getMessage(), msg.getDate());
                }
            else{
                if(msg.getFrom().getId().equals(user.getId())){
                    showReplyForSource(service.getMessage(msg.getReply()).getMessage(), msg.getMessage(), msg.getDate());
                }
                else{
                    this.showReplyAsForDestination(msg.getId(), service.getMessage(msg.getReply()).getMessage(), msg.getMessage(), msg.getDate());
                }
            }
        }
        labelChatWith.setText(labelChatWith.getText() + " " + userDestination.getUsername() + " - " + userDestination.getFirstName() + " " + userDestination.getLastName());
    }


    private void showReplyAsForDestination(Long id, String reply, String message, LocalDateTime date) {
        if(!message.isEmpty()) {
            HBox hBox1 = new HBox();
            hBox1.setAlignment(Pos.CENTER_LEFT);
            hBox1.setPadding(new Insets(5, 5, 5, 10));
            HBox hBox = new HBox();
            hBox.setAlignment(Pos.CENTER_LEFT);
            hBox.setPadding(new Insets(5, 5, 5, 10));

            javafx.scene.text.Text text = new Text(message);
            TextFlow textFlow = new TextFlow(text);
            textFlow.setStyle("-fx-color: rgb(0,0,0)" +
                    ";-fx-background-color: rgb(233,233,260)" +
                    ";-fx-background-radius: 20px");
            textFlow.setPadding(new Insets(5, 10, 5, 10));
            text.setFill(Color.color(0, 0, 0));
            javafx.scene.text.Text text1 = new Text(reply);
            TextFlow textFlow1 = new TextFlow(text1);
            textFlow1.setStyle("-fx-color: rgb(239,242,255)" +
                    ";-fx-background-color: rgb(15,125,242)" +
                    ";-fx-background-radius: 20px");
            textFlow1.setPadding(new Insets(5, 10, 5, 10));
            textFlow1.setOpacity(0.5);
            text1.setFill(Color.color(0.934,0.945, 0.996));
            hBox1.getChildren().add(new Label( this.userDestination.getUsername() + " replied to you: "));
            hBox1.getChildren().add(textFlow1);
            hBox.getChildren().add(textFlow);
            vBoxMessage.getChildren().add(hBox);
            textFieldMessage.clear();

            Button replyBtn = new Button("->");
            replyBtn.setStyle("-fx-background-color: Pink");
            replyBtn.setOpacity(0.4);
            replyBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event){
                    xButton.setVisible(true);
                    replyMessageId = id;
                    scrollPaneReply.setVisible(true);
                    HBox hBox1 = new HBox();
                    hBox1.setAlignment(Pos.CENTER_LEFT);
                    hBox1.setPadding(new Insets(5, 5, 5, 10));

                    javafx.scene.text.Text text = new Text(service.getMessage(id).getMessage());
                    TextFlow textFlow = new TextFlow(text);
                    textFlow.setStyle("-fx-color: rgb(0,0,0)" +
                            ";-fx-background-color: rgb(233,233,260)" +
                            ";-fx-background-radius: 20px");
                    textFlow.setPadding(new Insets(5, 10, 5, 10));
                    text.setFill(Color.color(0,0, 0));
                    hBox1.getChildren().add(new Label("Reply to: "));
                    hBox1.getChildren().add(textFlow);
                    vBoxReply.getChildren().setAll(hBox1);
                }
            });
            VBox vBoxReplyPack = new VBox();
            hBox.getChildren().add(replyBtn);
            vBoxReplyPack.getChildren().add(hBox1);
            vBoxReplyPack.getChildren().add(hBox);
            vBoxReplyPack.setStyle("-fx-background-color: #c5b3e8"+
                    ";-fx-border-color: #7b7091");
            HBox hBoxDateTime = new HBox();
            hBoxDateTime.setAlignment(Pos.TOP_LEFT);
            hBoxDateTime.setPadding(new Insets(5, 5, 5, 10));
            Label dateLabel = new Label(date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
            dateLabel.setFont(Font.font("Arial", 9));
            hBoxDateTime.getChildren().add(dateLabel);
            vBoxReplyPack.getChildren().add(hBoxDateTime);
            vBoxMessage.getChildren().add(vBoxReplyPack);
            textFieldMessage.clear();
        }
    }


    private void showReplyForSource(String reply, String message, LocalDateTime date) {
        if(!message.isEmpty()) {
            HBox hBox1 = new HBox();
            hBox1.setAlignment(Pos.CENTER_RIGHT);
            hBox1.setPadding(new Insets(5, 5, 5, 10));
            HBox hBox = new HBox();
            hBox.setAlignment(Pos.CENTER_RIGHT);
            hBox.setPadding(new Insets(5, 5, 5, 10));

            javafx.scene.text.Text text = new Text(message);
            TextFlow textFlow = new TextFlow(text);
            textFlow.setStyle("-fx-color: rgb(239,242,255)" +
                    ";-fx-background-color: rgb(15,125,242)" +
                    ";-fx-background-radius: 20px");
            textFlow.setPadding(new Insets(5, 10, 5, 10));
            text.setFill(Color.color(0.934,0.945, 0.996));

            javafx.scene.text.Text text1 = new Text(reply);
            TextFlow textFlow1 = new TextFlow(text1);
            textFlow1.setStyle("-fx-color: rgb(0,0,0)" +
                    ";-fx-background-color: rgb(233,233,260)" +
                    ";-fx-background-radius: 20px");
            textFlow1.setPadding(new Insets(5, 10, 5, 10));
            textFlow1.setOpacity(0.5);
            text1.setFill(Color.color(0, 0, 0));

            hBox1.getChildren().add(new Label("You replied to " + this.userDestination.getUsername() + ": "));
            hBox1.getChildren().add(textFlow1);
            hBox.getChildren().add(textFlow);
            vBoxMessage.getChildren().add(hBox);
            textFieldMessage.clear();
            VBox vBoxReplyPack = new VBox();
            vBoxReplyPack.getChildren().add(hBox1);
            vBoxReplyPack.getChildren().add(hBox);
            vBoxReplyPack.setStyle("-fx-background-color: #c5b3e8"+
                    ";-fx-border-color: #7b7091");
            HBox hBoxDateTime = new HBox();
            hBoxDateTime.setAlignment(Pos.TOP_RIGHT);
            hBoxDateTime.setPadding(new Insets(5, 5, 5, 10));
            Label dateLabel = new Label(date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
            dateLabel.setFont(Font.font("Arial", 9));
            hBoxDateTime.getChildren().add(dateLabel);
            vBoxReplyPack.getChildren().add(hBoxDateTime);
            vBoxMessage.getChildren().add(vBoxReplyPack);
            textFieldMessage.clear();
        }

    }

}
