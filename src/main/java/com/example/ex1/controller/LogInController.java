package com.example.ex1.controller;

import com.example.ex1.domain.*;
import com.example.ex1.domain.utils.HashPassword;
import com.example.ex1.domain.validators.*;
import com.example.ex1.service.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class LogInController {
    private Service service;
    private Stage curentStage;

    @FXML
    private ImageView logo;

    @FXML
    private TextField textFieldUserName;

    @FXML
    private PasswordField textFieldPassword;

    @FXML
    private Button buttonLogIn;

    @FXML
    private Label labelTest;

    @FXML
    private Button buttonSignIn;

    @FXML
    private void clickButtonSignIn(){
        try{
            FXMLLoader loader=new FXMLLoader(getClass().getResource("/views/signIn-view.fxml"));
            AnchorPane root = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.setResizable(false);
            SignInController controller=loader.getController();
            controller.initialize();
            controller.setServiceAndMenuStage(this.service, curentStage, stage);
            stage.setScene(new Scene(root));
            stage.setTitle("VapourWave SIGN IN");
            stage.getIcons().add(new Image("/Images/icons8-wave-64 (1).png"));
            curentStage.close();
            stage.show();


        }
        catch(ValidationException e){
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void clickButtonLogIn(){
        try{
            if(textFieldUserName.getText().isEmpty()){
                throw new ValidationException(" User Name Field can't be empty!");
            }
            if(textFieldPassword.getText().isEmpty()){
                throw new ValidationException(" Password Field can't be empty!");
            }
            User user = service.getUserByUserName(textFieldUserName.getText());
            if(user!=null){
                if(user.getPassword().equals(HashPassword.doHashing(textFieldPassword.getText()))){
                    this.curentStage.close();
                    this.showPage(user);
                }
                else{
                    MessageAlert.showErrorMessage(null, " Wrong password!");
                }
            }
            else{
                MessageAlert.showErrorMessage(null, " The User with user name " + textFieldUserName.getText() + " does not exist!");
            }
        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
        catch (IOException e){
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void initialize(String url, Stage curentStage){

        this.logo = new ImageView(url);
        this.curentStage = curentStage;
    }

    public void setService(Service service){
        this.service = service;
    }

    public void showPage(User user) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/views/page.fxml"));
        BorderPane root = (BorderPane) loader.load();
        Stage stage = new Stage();
        stage.setResizable(false);

        PageController controller=loader.getController();

        stage.setScene(new Scene(root));
        stage.setTitle("VapourWave Social Network");
        stage.getIcons().add(new Image("/Images/icons8-wave-64 (1).png"));
        controller.setAtributes(service, user, stage, curentStage, root);
        controller.initialize(user);
        stage.show();
    }
}