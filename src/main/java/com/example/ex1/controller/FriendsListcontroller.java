package com.example.ex1.controller;

import com.example.ex1.domain.*;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.service.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendsListcontroller {

    private Service service;
    private User user;
    private Label labelNumberOfFriends;
    ObservableList<FriendshipDto> users = FXCollections.observableArrayList();

    @FXML
    private TableView<FriendshipDto> tableViewFriends;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnUserName;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnFirstName;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnLastName;
    @FXML
    private TableColumn<FriendshipDto, LocalDate> tableColumnFriendsSince;
    @FXML
    private TextField textFieldSearch;
    @FXML
    private Button buttonRemoveFriend;


    public void initialize(){
        tableColumnUserName.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("username"));
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("lastName"));
        tableColumnFriendsSince.setCellValueFactory(new PropertyValueFactory<FriendshipDto, LocalDate>("date"));

        tableViewFriends.setItems(users);

        textFieldSearch.textProperty().addListener(o -> handleFilter());
    }

    public void setAtributes(Service service, User user, Label labelFriends){
        this.service = service;
        this.user = user;
        users.setAll(this.getFriendshipDtoList());
        this.labelNumberOfFriends =labelFriends;
    }

    private List<FriendshipDto> getFriendshipDtoList(){
        List<FriendshipDto> friendshipsToReturn = StreamSupport.stream(this.service.getAllFriendshipsOfAnUser(user.getId()).spliterator(), false)
                .map(x->new FriendshipDto(x.getLeft().getUsername(), x.getLeft().getFirstName(), x.getLeft().getLastName(), x.getRight()))
                .collect(Collectors.toList());
        return friendshipsToReturn;
    }

    private void handleFilter() {
        Predicate<FriendshipDto> p1 = n -> n.getUsername().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getLastName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getFirstName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase());
        users.setAll(getFriendshipDtoList()
                .stream()
                .filter(p1)
                .collect(Collectors.toList()));
    }

    @FXML
    private void clickButtonRemoveFriend(){
        try{
            FriendshipDto fr = this.tableViewFriends.getSelectionModel().getSelectedItem();
            if(this.tableViewFriends.getSelectionModel().getSelectedItem() == null){
                throw new ValidationException(" No friend was selected!");
            }
            Long id1 = this.user.getId();
            Long id2 = this.service.getUserByUserName(fr.getUsername()).getId();
            service.removeFriend(id1, id2);
            this.users.remove(fr);
            this.labelNumberOfFriends.setText(" Friends: " + service.getNumberOfFrendships(user.getId()));
        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    @FXML
    private void clickButtonInfo(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO","   * Here are the users that are your friends!");
    }


}
