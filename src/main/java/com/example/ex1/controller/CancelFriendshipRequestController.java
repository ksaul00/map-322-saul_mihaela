package com.example.ex1.controller;

import com.example.ex1.domain.FriendshipRequestDestinationDto;
import com.example.ex1.domain.User;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.service.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CancelFriendshipRequestController {

    private Service service;
    private User user;
    ObservableList<FriendshipRequestDestinationDto> sentFriendshipRequests = FXCollections.observableArrayList();

    @FXML
    private TableView<FriendshipRequestDestinationDto> tableViewSentFriendshipRequests;
    @FXML
    private TableColumn<FriendshipRequestDestinationDto, String> columnUserName;
    @FXML
    private TableColumn<FriendshipRequestDestinationDto, String> columnFirstName;
    @FXML
    private TableColumn<FriendshipRequestDestinationDto, String> columnLastName;
    @FXML
    private TableColumn<FriendshipRequestDestinationDto, String> columnStatus;
    @FXML
    private TableColumn<FriendshipRequestDestinationDto, LocalDate> columnDate;
    @FXML
    private TextField textFieldSearch;
    @FXML
    private Button buttonCancel;

    @FXML
    private void clickButtonInfo(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"Info","   * Here are all the friendship requests you sent.");
    }
    @FXML
    private void clickButtonCancel(){
        try{
            FriendshipRequestDestinationDto fr = this.tableViewSentFriendshipRequests.getSelectionModel().getSelectedItem();
            if(this.tableViewSentFriendshipRequests.getSelectionModel().getSelectedItem() == null){
                throw new ValidationException(" No friendship request was selected!");
            }
            User userDestination = service.getUserByUserName(fr.getToUserName());
            Long idDestination = userDestination.getId();
            Long idSource = user.getId();
            Long idFriendshipRequest = this.service.getIdFriendshipRequestBetweenTwoUsers(idSource, idDestination);

            this.service.removeFriendshipRequest(idFriendshipRequest);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO"," Request was canceled succesfully!");
            this.sentFriendshipRequests.remove(fr);
        }
        catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }


    public void initialize(){
        columnUserName.setCellValueFactory(new PropertyValueFactory<FriendshipRequestDestinationDto, String>("toUserName"));
        columnFirstName.setCellValueFactory(new PropertyValueFactory<FriendshipRequestDestinationDto, String>("toFirstName"));
        columnLastName.setCellValueFactory(new PropertyValueFactory<FriendshipRequestDestinationDto, String>("toLastName"));
        columnStatus.setCellValueFactory(new PropertyValueFactory<FriendshipRequestDestinationDto, String>("status"));
        columnDate.setCellValueFactory(new PropertyValueFactory<FriendshipRequestDestinationDto, LocalDate>("date"));

        tableViewSentFriendshipRequests.setItems(sentFriendshipRequests);

        textFieldSearch.textProperty().addListener(o -> handleFilter());
    }

    public void setAtributes(Service service, User user){
        this.service = service;
        this.user = user;
        sentFriendshipRequests.setAll(this.getSentFriendshipRequests());
    }

    private List<FriendshipRequestDestinationDto> getSentFriendshipRequests(){
        List<FriendshipRequestDestinationDto> sentFriendshipRequestsToReturn = StreamSupport.stream(this.service.getFriendshipRequestsSentByAnUser(user.getId()).spliterator(), false)
                .map(x->{
                    User user1 = service.getUser(x.getIdDestination());
                    return new FriendshipRequestDestinationDto(user1.getUsername(), user1.getFirstName(), user1.getLastName(), x.getStatusFriendshipRequest().toString(), x.getDate());
                })
                .collect(Collectors.toList());
        return sentFriendshipRequestsToReturn;
    }

    private void handleFilter() {
        Predicate<FriendshipRequestDestinationDto> p1 = n -> n.getToUserName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getToLastName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getToFirstName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase());
        sentFriendshipRequests.setAll(getSentFriendshipRequests()
                .stream()
                .filter(p1)
                .collect(Collectors.toList()));
    }
}
