package com.example.ex1.controller;

import com.example.ex1.domain.*;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.service.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ShowPendingFriendshipRequestsController {

    private Service service;
    private User user;
    private Label labelNumberOfFriends;
    private Label labelPendingRequests;
    ObservableList<FriendshipRequestSourceDto> pendingFriendshipRequests = FXCollections.observableArrayList();

    @FXML
    private TableView<FriendshipRequestSourceDto> tableViewPendingFriendshipRequests;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, String> columnUserName;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, String> columnFirstName;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, String> columnLastName;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, String> columnStatus;
    @FXML
    private TableColumn<FriendshipRequestSourceDto, LocalDate> columnDate;
    @FXML
    private TextField textFieldSearch;
    @FXML
    private Button buttonAccept;
    @FXML
    private Button buttonReject;

    @FXML
    private void clickButtonInfo(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO","   * Here are all your PENDING friendship requests.");
    }
    @FXML
    private void clickButtonAccept(){
        try{
            FriendshipRequestSourceDto fr = this.tableViewPendingFriendshipRequests.getSelectionModel().getSelectedItem();
            if(this.tableViewPendingFriendshipRequests.getSelectionModel().getSelectedItem() == null){
                throw new ValidationException(" No friendship request was selected!");
            }
            User userSource = service.getUserByUserName(fr.getFromUserName());
            Long idSource = userSource.getId();
            Long idDestination = user.getId();
            Long idFriendshipRequest = this.service.getIdFriendshipRequestBetweenTwoUsers(idSource, idDestination);
            this.service.addFriend(idSource, idDestination);
            this.service.updateStatusFriendshipRequest(service.getFriendshipRequest(idFriendshipRequest), "APPROVED");
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO"," * Request was approved succesfully! \n * Now you and " + userSource.getUsername() + " are friends!");
            this.pendingFriendshipRequests.remove(fr);
            this.labelNumberOfFriends.setText(" Friends: " + service.getNumberOfFrendships(user.getId()));
            this.labelPendingRequests.setText(" Pending Requests: " + pendingFriendshipRequests.size());
        }
        catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }
    @FXML
    private void clickButtonReject(){
        try{
            FriendshipRequestSourceDto fr = this.tableViewPendingFriendshipRequests.getSelectionModel().getSelectedItem();
            if(this.tableViewPendingFriendshipRequests.getSelectionModel().getSelectedItem() == null){
                throw new ValidationException(" No friendship request was selected!");
            }
            User userSource = service.getUserByUserName(fr.getFromUserName());
            Long idSource = userSource.getId();
            Long idDestination = user.getId();
            Long idFriendshipRequest = this.service.getIdFriendshipRequestBetweenTwoUsers(idSource, idDestination);
            this.service.updateStatusFriendshipRequest(service.getFriendshipRequest(idFriendshipRequest), "REJECTED");
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO"," Request was rejected!");
            this.pendingFriendshipRequests.remove(fr);
            this.labelPendingRequests.setText(" Pending Requests: " + pendingFriendshipRequests.size());
        }
        catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    public void initialize(){
        columnUserName.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, String>("fromUserName"));
        columnFirstName.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, String>("fromFirstName"));
        columnLastName.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, String>("fromLastName"));
        columnStatus.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, String>("status"));
        columnDate.setCellValueFactory(new PropertyValueFactory<FriendshipRequestSourceDto, LocalDate>("date"));

        tableViewPendingFriendshipRequests.setItems(pendingFriendshipRequests);

        textFieldSearch.textProperty().addListener(o -> handleFilter());
    }

    public void setAtributes(Service service, User user, Label labelNumberOfFriends, Label labelRequests){
        this.service = service;
        this.user = user;
        pendingFriendshipRequests.setAll(this.getPendingFriendshipRequests());
        this.labelNumberOfFriends = labelNumberOfFriends;
        this.labelPendingRequests = labelRequests;
    }

    private List<FriendshipRequestSourceDto> getPendingFriendshipRequests(){
        List<FriendshipRequestSourceDto> friendshipRequestsToReturn = StreamSupport.stream(this.service.getPendingFriendshipRequestsOfAnUser(user.getId()).spliterator(), false)
                .map(x->{
                    User user1 = service.getUser(x.getIdSource());
                    return new FriendshipRequestSourceDto(user1.getUsername(), user1.getFirstName(), user1.getLastName(), x.getStatusFriendshipRequest().toString(), x.getDate());
                })
                .collect(Collectors.toList());
        return friendshipRequestsToReturn;
    }

    private void handleFilter() {
        Predicate<FriendshipRequestSourceDto> p1 = n -> n.getFromUserName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getFromLastName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getFromFirstName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase());
        pendingFriendshipRequests.setAll(getPendingFriendshipRequests()
                .stream()
                .filter(p1)
                .collect(Collectors.toList()));
    }
}
