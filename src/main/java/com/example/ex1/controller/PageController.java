package com.example.ex1.controller;

import com.example.ex1.domain.User;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.service.Service;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PageController {
    private Service service;
    private User user;
    private Stage curentStage;
    private Stage logInStage;
    private List<Button> listaButoane;
    private BorderPane borderPane;

    @FXML
    private Label labelUserName;
    @FXML
    private Label labelFirstName;
    @FXML
    private Label labelLastName;
    @FXML
    private Button buttonLogOut;
    @FXML
    private Button buttonFriendsList;
    @FXML
    private Button buttonAddFriend;
    @FXML
    private Button buttonShowFriendshipRequestHistory;
    @FXML
    private Button buttonPendingFriendshipRequests;
    @FXML
    private Button buttonCancelASentFriendshipRequest;
    @FXML
    private Button buttonChat;
    @FXML
    private Button buttonSendAMessageToManyFriends;
    @FXML
    private Label labelNumberOfFriends;
    @FXML
    private Label labelPendingRequests;
    @FXML
    private Button buttonDeleteAccount;

    @FXML
    private void clickButtonDeleteAccount(){
        try{
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(" Delete Account Confirmation ");
            alert.setContentText(" * This action will delete your account! Press OK to continue ");
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image("/Images/icons8-wave-64 (1).png"));
            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                service.removeUser(user.getId());
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO"," * Your account was deleted succesfully!");
                this.curentStage.close();
                this.logInStage.show();
            }
            else {
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO"," * The action was canceled!");
            }

        }
        catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    @FXML
    private void clickButtonFriendsList() throws IOException {
        setClickedCollor(buttonFriendsList);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/friendsList-view.fxml"));
        AnchorPane root=fxmlLoader.load();
        FriendsListcontroller controller=fxmlLoader.getController();
        controller.setAtributes(service, user, labelNumberOfFriends);
        this.borderPane.setCenter(root);
    }
    @FXML
    private void clickButtonAddFriend() throws IOException {
        setClickedCollor(buttonAddFriend);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/addFriend-view.fxml"));
        AnchorPane root=fxmlLoader.load();
        AddFriendController controller=fxmlLoader.getController();
        controller.setAtributes(service, user);
        this.borderPane.setCenter(root);
    }
    @FXML
    private void clickButtonShowFriendshipRequestHistory() throws IOException {
        setClickedCollor(buttonShowFriendshipRequestHistory);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/showFriendshipRequestHistory-view.fxml"));
        AnchorPane root=fxmlLoader.load();
        ShowFriendshipRequestHistoryController controller=fxmlLoader.getController();
        controller.setAtributes(service, user);
        this.borderPane.setCenter(root);
    }
    @FXML
    private void clickButtonPendingFriendshipRequests() throws IOException {
        setClickedCollor(buttonPendingFriendshipRequests);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/showPendingFriendshipRequests-view.fxml"));
        AnchorPane root=fxmlLoader.load();
        ShowPendingFriendshipRequestsController controller=fxmlLoader.getController();
        controller.setAtributes(service, user, labelNumberOfFriends, labelPendingRequests);
        this.borderPane.setCenter(root);
    }
    @FXML
    private void clickButtonCancelASentFriendshipRequest() throws IOException {
        setClickedCollor(buttonCancelASentFriendshipRequest);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/cancelASentFriendshipRequest-view.fxml"));
        AnchorPane root=fxmlLoader.load();
        CancelFriendshipRequestController controller=fxmlLoader.getController();
        controller.setAtributes(service, user);
        this.borderPane.setCenter(root);
    }
    @FXML
    private void clickButtonChat() throws IOException {

        setClickedCollor(buttonChat);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/chatMenu.fxml"));
        AnchorPane root=fxmlLoader.load();
        MenuChatController controller=fxmlLoader.getController();
        controller.setAtributes(service, user, borderPane, root);
        this.borderPane.setCenter(root);
    }
    @FXML
    private void clickButtonSendAMessageToManyFriends() throws IOException {
        setClickedCollor(buttonSendAMessageToManyFriends);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/sendAMessageToManyFrinds.fxml"));
        AnchorPane root=fxmlLoader.load();
        SendAMessageToManyFriendsController controller=fxmlLoader.getController();
        controller.setAtributes(service, user);
        this.borderPane.setCenter(root);
    }

    @FXML
    private void clickButtonLogOut(){
        this.curentStage.close();
        this.logInStage.show();
    }

    public void setAtributes(Service service, User user, Stage curentStage, Stage logInStage, BorderPane borderPane){
        this.service = service;
        this.user = user;
        this.curentStage = curentStage;
        this.logInStage = logInStage;
        this.borderPane = borderPane;
    }

    public void initialize(User user) throws IOException {
        this.labelUserName.setText(user.getUsername());
        this.labelFirstName.setText(user.getFirstName());
        this.labelLastName.setText(user.getLastName());
        this.listaButoane = new ArrayList<Button>();
        this.listaButoane.add(buttonFriendsList);
        this.listaButoane.add(buttonAddFriend);
        this.listaButoane.add(buttonShowFriendshipRequestHistory);
        this.listaButoane.add(buttonPendingFriendshipRequests);
        this.listaButoane.add(buttonCancelASentFriendshipRequest);
        this.listaButoane.add(buttonChat);
        this.listaButoane.add(buttonSendAMessageToManyFriends);
        this.labelNumberOfFriends.setText(" Friends: " + service.getNumberOfFrendships(user.getId()));
        this.labelPendingRequests.setText(" Pending Requests: " + service.getNumberOfPendngRequests(user.getId()));
        this.clickButtonFriendsList();
    }

    private void setClickedCollor(Button button){
        for(Button bt: listaButoane){
            if(bt.equals(button)){
                bt.setStyle("-fx-background-color: #9355A1; -fx-border-color: black");
            }
            else {
                bt.setStyle("-fx-background-color:  #bfa5e6; -fx-border-color: black");
            }
        }
    }
}
