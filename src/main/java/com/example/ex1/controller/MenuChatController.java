package com.example.ex1.controller;

import com.example.ex1.domain.FriendshipDto;
import com.example.ex1.domain.User;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.service.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MenuChatController {

    private Service service;
    private User user;
    private BorderPane mainPage;
    private AnchorPane menuChat;
    ObservableList<FriendshipDto> users = FXCollections.observableArrayList();

    @FXML
    private TableView<FriendshipDto> tableViewFriends;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnUserName;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnFirstName;
    @FXML
    private TableColumn<FriendshipDto, String> tableColumnLastName;
    @FXML
    private TableColumn<FriendshipDto, LocalDate> tableColumnFriendsSince;
    @FXML
    private TextField textFieldSearch;
    @FXML
    private Button buttonStartChat;


    public void initialize(){
        tableColumnUserName.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("username"));
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<FriendshipDto, String>("lastName"));
        tableColumnFriendsSince.setCellValueFactory(new PropertyValueFactory<FriendshipDto, LocalDate>("date"));

        tableViewFriends.setItems(users);

        textFieldSearch.textProperty().addListener(o -> handleFilter());
    }

    public void setAtributes(Service service, User user, BorderPane page, AnchorPane menuChat){
        this.menuChat = menuChat;
        this.service = service;
        this.user = user;
        users.setAll(this.getFriendshipDtoList());
        this.mainPage = page;
    }

    private List<FriendshipDto> getFriendshipDtoList(){
        List<FriendshipDto> friendshipsToReturn = StreamSupport.stream(this.service.getAllFriendshipsOfAnUser(user.getId()).spliterator(), false)
                .map(x->new FriendshipDto(x.getLeft().getUsername(), x.getLeft().getFirstName(), x.getLeft().getLastName(), x.getRight()))
                .collect(Collectors.toList());
        return friendshipsToReturn;
    }

    private void handleFilter() {
        Predicate<FriendshipDto> p1 = n -> n.getUsername().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getLastName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase()) ||
                n.getFirstName().toUpperCase().startsWith(textFieldSearch.getText().toUpperCase());
        users.setAll(getFriendshipDtoList()
                .stream()
                .filter(p1)
                .collect(Collectors.toList()));
    }

    @FXML
    private void clickButtonStartChat(){
        try{
            FriendshipDto fr = this.tableViewFriends.getSelectionModel().getSelectedItem();
            if(this.tableViewFriends.getSelectionModel().getSelectedItem() == null){
                throw new ValidationException(" No friend was selected");
            }
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/views/chat-view.fxml"));
            AnchorPane root=fxmlLoader.load();
            ChatController controller=fxmlLoader.getController();
            controller.setAll(service, user, this.service.getUserByUserName(fr.getUsername()), mainPage, menuChat);
            this.mainPage.setCenter(root);

        } catch (ValidationException | IOException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    @FXML
    private void clickButtonInfo(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"INFO","   * Here are the users that are your friends!");
    }


}
