package com.example.ex1.controller;

import com.example.ex1.domain.User;
import com.example.ex1.domain.validators.UtilizatorValidator;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.service.Service;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SignInController {
    private Service service;
    private Stage menuStage;
    private Stage currentStage;

    @FXML
    private TextField textFieldFirstName;

    @FXML
    private TextField textFieldLastName;

    @FXML
    private TextField textFieldUserName;

    @FXML
    private PasswordField passwordFieldPassword;

    @FXML
    private PasswordField passwordFieldConfirmPassword;

    @FXML
    private Button buttonBack;

    @FXML
    private Button buttonInfoFirstName;

    @FXML
    private Button buttonInfoLastName;

    @FXML
    private Button buttonInfoUserName;

    @FXML
    private Button buttonInfoPassword;

    @FXML
    private void clickButtonInfoFirstName(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"First Name", " * The First Name can contain only letters and only the first one is upper!");
    }

    @FXML
    private void clickButtonInfoLastName(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"Last Name", " * The Last Name can contain only letters and only the first one is upper");
    }

    @FXML
    private void clickButtonInfoUserName(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"User Name", " * The User Name must contain at least 5 characters, 3 letters and it can't contain spaces!");
    }

    @FXML
    private void clickButtonInfoPassword(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"Password", " * The password must contain at least 8 characters: \n - mininum one upper letter; \n - minimum 3 letters; \n - minimum 2 digit numbers!");
    }

    @FXML
    private void clickButtonSignIn(){
        try{
            String firstName = textFieldFirstName.getText();
            String lastName = textFieldLastName.getText();
            String userName = textFieldUserName.getText();
            String password = passwordFieldPassword.getText();
            String confirmPassword = passwordFieldConfirmPassword.getText();

            if(textFieldFirstName.getText().isEmpty()){
                throw new ValidationException(" First Name Field can't be empty!");
            }
            if(textFieldLastName.getText().isEmpty()){
                throw new ValidationException(" Last Name Field can't be empty!");
            }
            if(textFieldUserName.getText().isEmpty()){
                throw new ValidationException(" User Name Field can't be empty!");
            }
            if(passwordFieldPassword.getText().isEmpty()){
                throw new ValidationException(" Password Field can't be empty!");
            }
            if(passwordFieldConfirmPassword.getText().isEmpty()){
                throw new ValidationException(" Confirm Password Field can't be empty!");
            }
            if(service.getUserByUserName(userName)!=null){
                throw new ValidationException(" An user with UserName " + userName + " already exists!");
            }
            if(!password.equals(confirmPassword)){
                throw new ValidationException(" Password and Confirm Password do not match!");
            }
            UtilizatorValidator.validatePassword(password);

            User task = this.service.addUser(userName, firstName, lastName, password);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"SIGN IN Status"," UserName: " + task.getUsername() + ", User: " + task.getFirstName() + " " + task.getLastName() + " succesfully added!");

            this.currentStage.close();
            this.menuStage.show();
        }
        catch(ValidationException e){
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    @FXML
    private void clickButtonBack(){
        this.currentStage.close();
        this.menuStage.show();
    }

    public void initialize(){}

    public void setServiceAndMenuStage(Service service, Stage menuStage, Stage currentStage){
        this.service = service;
        this.menuStage = menuStage;
        this.currentStage = currentStage;
    }
}
