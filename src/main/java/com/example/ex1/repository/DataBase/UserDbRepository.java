package com.example.ex1.repository.DataBase;


import com.example.ex1.domain.User;
import com.example.ex1.domain.utils.HashPassword;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.domain.validators.Validator;
import com.example.ex1.repository.Repository;

import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class UserDbRepository implements Repository<Long, User> {
    private String url;
    private String username;
    private String password;
    private Validator<User> validator;

    public UserDbRepository(String url, String username, String password, Validator<User> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public User findOne(Long idEntiate) {
        if(idEntiate==null)
            throw new IllegalArgumentException(" The id cannot be null!");

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement("SELECT * from users1 where (id = ?)");
             ){
            ps.setLong(1, idEntiate);
            ResultSet resultSet = ps.executeQuery();
            if(resultSet.next()) {
                Long id = resultSet.getLong("id");
                String userName = resultSet.getString("username");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String password = resultSet.getString("password");

                User user = new User(userName, firstName, lastName, password);
                user.setId(id);
                return user;
            }
            else{
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<User> findAll() {
        HashMap<Long, User> users = new HashMap<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from users1");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String userName = resultSet.getString("username");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String password = resultSet.getString("password");
                User utilizator = new User(userName, firstName, lastName, password);
                utilizator.setId(id);
                users.put(id ,utilizator);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");

                users.get(id1).addFriend(users.get(id2));
                users.get(id2).addFriend(users.get(id1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users.values();

    }

    @Override
    public User save(User entity) {
        if(entity==null)
            throw new IllegalArgumentException(" The entity cannot be null!");
        validator.validate(entity);

        String sql = "insert into users1 (username, first_name, last_name, password) values (?, ?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getUsername());
            ps.setString(2, entity.getFirstName());
            ps.setString(3, entity.getLastName());
            ps.setString(4, HashPassword.doHashing(entity.getPassword()));

            ps.executeUpdate();
            entity.setId(null);
            return entity;
        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User delete(Long id) {
        if(id==null)
            throw new IllegalArgumentException(" The entity cannot be null!");
        User task = findOne(id);
        if(task!=null){
            String sql = "delete from users1 where id = ?";
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setLong(1, id);
                ps.executeUpdate();
                return task;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
        else{
            throw new ValidationException(" The User with ID: " + id + " does not exist!");
        }
    }

    @Override
    public User update(User entity) {
        if (entity == null)
            throw new IllegalArgumentException(" The entity must not be null!");
        validator.validate(entity);

        String sql = "update users1 set first_name = ?, last_name = ?  where id=?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());
            ps.setLong(3, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getSize() {
        int size=0;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT Count(*) AS NumarUsers from users1");
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                size=resultSet.getInt("NumarUsers");
            }
            return size;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return size;
    }

    @Override
    public Set<Long> getIDs() {
        Set<Long> ids = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT id from users1");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                ids.add(id);
            }
            return ids;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ids;
    }
}
