package com.example.ex1.repository.DataBase;


import com.example.ex1.domain.FriendshipRequest;
import com.example.ex1.domain.validators.ValidationException;
import com.example.ex1.domain.validators.Validator;
import com.example.ex1.repository.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class FriendShipRequestDbRepository implements Repository<Long, FriendshipRequest> {

    private String url;
    private String username;
    private String password;
    private Validator<FriendshipRequest> validator;

    public FriendShipRequestDbRepository(String url, String username, String password, Validator<FriendshipRequest> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public FriendshipRequest findOne(Long idEntiate) {
        if(idEntiate==null)
            throw new IllegalArgumentException(" The id cannot be null!");

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement("SELECT * from friendship_requests where (id = ?)");
        ){
            ps.setLong(1, idEntiate);
            ResultSet resultSet = ps.executeQuery();
            if(resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long id_source = resultSet.getLong("id_source");
                Long id_destination = resultSet.getLong("id_destination");
                String status_friendship_requests = resultSet.getString("status_friendship_request");
                LocalDate date = resultSet.getDate("date").toLocalDate();

                FriendshipRequest friendshipRequest = new FriendshipRequest(id_source, id_destination, status_friendship_requests, date);
                friendshipRequest.setId(id);
                return friendshipRequest;
            }
            else{
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<FriendshipRequest> findAll() {
        Set<FriendshipRequest> friendshipRequestSet = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendship_requests");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long id_source = resultSet.getLong("id_source");
                Long id_destination = resultSet.getLong("id_destination");
                String status_friendship_requests = resultSet.getString("status_friendship_request");
                LocalDate date = resultSet.getDate("date").toLocalDate();

                FriendshipRequest friendshipRequest = new FriendshipRequest(id_source, id_destination, status_friendship_requests, date);
                friendshipRequest.setId(id);
                friendshipRequestSet.add(friendshipRequest);
            }
            return friendshipRequestSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendshipRequestSet;
    }

    @Override
    public FriendshipRequest save(FriendshipRequest entity) {
        if(entity==null)
            throw new IllegalArgumentException(" The entity cannot be null!");
        validator.validate(entity);

        String sql = "insert into friendship_requests (id_source, id_destination, status_friendship_request, date) values (?, ?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, entity.getIdSource());
            ps.setLong(2, entity.getIdDestination());
            ps.setString(3, entity.getStatusFriendshipRequest().toString());
            ps.setDate(4, Date.valueOf(entity.getDate()));

            ps.executeUpdate();
            entity.setId(null);
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendshipRequest delete(Long id) {
        if(id==null)
            throw new IllegalArgumentException(" The entity cannot be null!");
        FriendshipRequest task = findOne(id);
        if(task!=null){
            String sql = "delete from friendship_requests where id = ?";
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setLong(1, id);
                ps.executeUpdate();
                return task;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
        else{
            throw new ValidationException(" The Friend Request with ID: " + id + " does not exist!");
        }
    }

    @Override
    public FriendshipRequest update(FriendshipRequest entity) {
        if (entity == null)
            throw new IllegalArgumentException(" The entity must be not null!");
        validator.validate(entity);

        String sql = "update friendship_requests set status_friendship_request = ?  where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, entity.getStatusFriendshipRequest().toString());
            ps.setLong(2, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getSize() {
        int size=0;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT Count(*) AS NumarFriendshipRequests from friendship_requests");
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                size=resultSet.getInt("NumarFriendshipRequests");
            }
            return size;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return size;
    }

    @Override
    public Set<Long> getIDs() {
        return null;
    }
}
