package com.example.ex1;

import com.example.ex1.controller.LogInController;
import com.example.ex1.domain.*;
import com.example.ex1.domain.validators.FriendShipRequestValidator;
import com.example.ex1.domain.validators.FriendShipValidator;
import com.example.ex1.domain.validators.MessageValidator;
import com.example.ex1.domain.validators.UtilizatorValidator;
import com.example.ex1.repository.DataBase.FriendShipDbRepository;
import com.example.ex1.repository.DataBase.FriendShipRequestDbRepository;
import com.example.ex1.repository.DataBase.MessageDataBase;
import com.example.ex1.repository.DataBase.UserDbRepository;
import com.example.ex1.repository.Repository;
import com.example.ex1.service.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApp extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        Repository<Long, User> userRepository;
        Repository<Tuple<Long, Long>, FriendShip> friendRepository;
        Repository<Long, FriendshipRequest> friendshipRequestRepository;
        Repository<Long, Message> messageRepository;

        userRepository = new UserDbRepository("jdbc:postgresql://localhost:5432/lab", "postgres", "parola1234", new UtilizatorValidator());
        friendRepository = new FriendShipDbRepository("jdbc:postgresql://localhost:5432/lab", "postgres", "parola1234", new FriendShipValidator());
        friendshipRequestRepository = new FriendShipRequestDbRepository("jdbc:postgresql://localhost:5432/lab", "postgres", "parola1234", new FriendShipRequestValidator());
        messageRepository = new MessageDataBase("jdbc:postgresql://localhost:5432/lab", "postgres", "parola1234", new MessageValidator(), userRepository);

        UserService userService = new UserService(userRepository);
        FriendShipService friendShipService = new FriendShipService(friendRepository);
        FriendShipRequestService friendShipRequestService = new FriendShipRequestService(friendshipRequestRepository);
        MessageService messageService = new MessageService(messageRepository);

        Service service = new Service(userService, friendShipService, friendShipRequestService, messageService);

        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/logIn-view.fxml"));

        AnchorPane root=loader.load();

        LogInController controller=loader.getController();
        controller.setService(service);

        primaryStage.setScene(new Scene(root, 800, 275));
        primaryStage.setTitle("VapourWave LOG IN");
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("/Images/icons8-wave-64 (1).png"));

        controller.initialize("Images/Great-Wave-1.jpg", primaryStage);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}