package com.example.ex1.domain;

public enum StatusFrierndshipRequest {
    PENDING,
    APPROVED,
    REJECTED;
}
