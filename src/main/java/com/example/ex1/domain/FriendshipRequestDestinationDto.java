package com.example.ex1.domain;

import java.time.LocalDate;

public class FriendshipRequestDestinationDto {
    private String toUserName;
    private String toFirstName;
    private String toLastName;
    private String status;
    private LocalDate date;

    public FriendshipRequestDestinationDto(String toUserName, String toFirstName, String toLastName, String status, LocalDate date) {
        this.toUserName = toUserName;
        this.toFirstName = toFirstName;
        this.toLastName = toLastName;
        this.status = status;
        this.date = date;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getToFirstName() {
        return toFirstName;
    }

    public void setToFirstName(String toFirstName) {
        this.toFirstName = toFirstName;
    }

    public String getToLastName() {
        return toLastName;
    }

    public void setToLastName(String toLastName) {
        this.toLastName = toLastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FriendshipRequestDestinationDto{" +
                "toUserName='" + toUserName + '\'' +
                ", toFirstName='" + toFirstName + '\'' +
                ", toLastName='" + toLastName + '\'' +
                ", status='" + status + '\'' +
                ", date=" + date +
                '}';
    }
}
