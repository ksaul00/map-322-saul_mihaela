package com.example.ex1.domain;

import java.time.LocalDate;

public class FriendshipDto {
    private String username;
    private String firstName;
    private String lastName;
    private LocalDate date;

    public FriendshipDto(String username, String firstName, String lastName, LocalDate date) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FriendshipDto{" +
                "username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
