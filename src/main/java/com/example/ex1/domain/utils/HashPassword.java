package com.example.ex1.domain.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashPassword {
    public static String doHashing(String password) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA");
        messageDigest.update(password.getBytes());
        byte[]  resultByteArray = messageDigest.digest();
        StringBuilder sb = new StringBuilder();
        for(byte b: resultByteArray){
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
}
