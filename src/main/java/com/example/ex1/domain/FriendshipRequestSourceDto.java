package com.example.ex1.domain;

import java.time.LocalDate;

public class FriendshipRequestSourceDto {
    private String fromUserName;
    private String fromFirstName;
    private String fromLastName;
    private String status;
    private LocalDate date;

    public FriendshipRequestSourceDto(String fromUserName, String fromFirstName, String fromLastName, String status, LocalDate date) {
        this.fromUserName = fromUserName;
        this.fromFirstName = fromFirstName;
        this.fromLastName = fromLastName;
        this.status = status;
        this.date = date;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getFromFirstName() {
        return fromFirstName;
    }

    public void setFromFirstName(String fromFirstName) {
        this.fromFirstName = fromFirstName;
    }

    public String getFromLastName() {
        return fromLastName;
    }

    public void setFromLastName(String fromLastName) {
        this.fromLastName = fromLastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FriendshipRequestDto{" +
                "fromUserName='" + fromUserName + '\'' +
                ", fromFirstName='" + fromFirstName + '\'' +
                ", fromLastName='" + fromLastName + '\'' +
                ", status='" + status + '\'' +
                ", date=" + date +
                '}';
    }
}
