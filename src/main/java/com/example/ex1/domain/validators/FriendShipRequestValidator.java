package com.example.ex1.domain.validators;

import com.example.ex1.domain.FriendshipRequest;

public class FriendShipRequestValidator implements Validator<FriendshipRequest>{
    @Override
    public void validate(FriendshipRequest entity) throws ValidationException {
        if(entity.getIdSource().equals(entity.getIdDestination())){
            throw new ValidationException(" An user can't be his own friend!");
        }
    }
}
