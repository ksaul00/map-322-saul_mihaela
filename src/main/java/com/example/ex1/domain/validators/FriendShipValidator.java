package com.example.ex1.domain.validators;


import com.example.ex1.domain.FriendShip;

public class FriendShipValidator implements Validator<FriendShip> {
    @Override
    public void validate(FriendShip entity) throws ValidationException {
        if(entity.getId().getLeft().equals(entity.getId().getRight())){
            throw new ValidationException("An user can't be his own friend!");
        }
    }
}
