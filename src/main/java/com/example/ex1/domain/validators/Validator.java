package com.example.ex1.domain.validators;

public interface Validator<T> {
    void validate(T entity) throws ValidationException;
}