module com.example.ex1 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires java.sql;

    opens com.example.ex1.domain to javafx.fxml, javafx.base;
    opens com.example.ex1.repository to javafx.fxml, javafx.base;
    opens com.example.ex1.service to javafx.fxml, javafx.base;

    opens com.example.ex1 to javafx.fxml;
    exports com.example.ex1;
    exports com.example.ex1.controller;
    opens com.example.ex1.controller to javafx.fxml, java.base;
}